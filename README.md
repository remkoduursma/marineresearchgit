
# Analysis of Almere parking data

**Example project for marine research training**


## Instructions

Install all packages from `R/load_packages.R`, then run the `script.R` to produce figures and data.

**Note:** this repository does not work, it is for demo purposes only.


## Contact

Remko Duursma and Collaborator
